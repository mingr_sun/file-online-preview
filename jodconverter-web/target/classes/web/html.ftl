<!DOCTYPE html>

<html lang="en">
<head>
    <style type="text/css">
        body{
            margin: 0;
            padding:0;
            border:0;
        }
    </style>
    <title>用友品牌共享平台</title>
    <link rel="stylesheet" type="text/css" href="css/doc.css"/>

</head>
<body style="width:100%;height:100%;">
    		<iframe src="${pdfUrl}" width="100%" frameborder="0" ></iframe>
</body>
<script type="text/javascript">
    document.getElementsByTagName('iframe')[0].height = document.documentElement.clientHeight-10;
    /**
     * 页面变化调整高度
     */
    window.onresize = function(){
        var fm = document.getElementsByTagName("iframe")[0];
        fm.height = window.document.documentElement.clientHeight-10;
    }
</script>
</html>